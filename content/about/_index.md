+++
title = "About & FAQ"
weight = 50
chapter = true
pre = "<b>4. </b>"
+++

# About & FAQ

### Who's using RainbowCake?

<img src="/images/autsoft.png" style="width:150px;"/>
<!-- margin-left:0 !important; -->

The development of RainbowCake started within [AutSoft](https://autsoft.hu/en), where the framework is being used to build greenfield Android/Kotlin projects (such as [Sender](https://play.google.com/store/apps/details?id=hu.posta.kepeslap)), and it also serves as the foundation for applications that are being refactored from Java to Kotlin (such as [NEXON-PORT+](https://play.google.com/store/apps/details?id=hu.nexon.nexonmobile)). Both of these examples are running on RainbowCake in production.

### Where's the source code? How can I contribute? Where do I open issues?

Some things are still being worked out, but you can start participating in the project [on GitHub](https://github.com/rainbowcake)!
